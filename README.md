# Opensocks-Qt for Desktop
> A fork and advanced version from Shadowsocks-Qt5.

 This version is only for desktop users, another repo can be used for mobile.  
 This project is avaliable in [GitHub](https://github.com/opensocks/opensocks-qtd) and [BitBucket](https://bitbucket.org/opensocks/opensocks-qtd).

## Special Thanks
Opensocks-Qt cannot without these helppers:  
[clowwindy](https://shadowsocks.org) The developer of the original project: Shadowsocks.  
[librehat](https://github.com/librehat) The developer of the Shadowsocks-Qt5 project.  

## License
![GNU Lesser General Public License v3.0](http://www.gnu.org/graphics/lgplv3-147x51.png)

This project is under [GNU General Public License v3.0](http://www.gnu.org/copyleft/lesser.html).